/***********************
* Adobe Edge Animate Composition Actions
*
* Edit this file with caution, being careful to preserve 
* function signatures and comments starting with 'Edge' to maintain the 
* ability to interact with these actions from within Adobe Edge Animate
*
***********************/
(function($, Edge, compId){
var Composition = Edge.Composition, Symbol = Edge.Symbol; // aliases for commonly used Edge classes

   //Edge symbol: 'stage'
   (function(symbolName) {
      
      
      

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 0, function(sym, e) {
         // insert code here
         sym.stop();
         statusFlag=0;

      });
      //Edge binding end

      

      

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 1500, function(sym, e) {
         // insert code here
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 3000, function(sym, e) {
         // insert code here
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 3650, function(sym, e) {
         // insert code here
         sym.play(12500);

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 4500, function(sym, e) {
         // insert code here
         sym.stop();
         statusFlag = 1;
         

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 6000, function(sym, e) {
         // insert code here
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 7500, function(sym, e) {
         // insert code here
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 9000, function(sym, e) {
         // insert code here
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 10500, function(sym, e) {
         // insert code here
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 11999, function(sym, e) {
         // insert code here
         //TEST
         sym.stop();
         statusFlag = 2;
         
         var totalNum = guestNum;
         var thousand = parseInt(totalNum/1000);
         var hundred = parseInt((totalNum%1000)/100);
         var ten = parseInt((totalNum%100)/10);
         var one = totalNum%10;
         
         th = 0;
         hu = 6;
         te = 1;
         on = 9;
         
         var clockNum = setInterval(function (){
         	if(th == thousand){}
         	else{ th++; sym.$("Thousand").html(th);}
         
         	if(hu == hundred + 10){}
         	else{ hu++; sym.$("Hundred").html(hu%10);}
         
         	if(te == ten + 20){}
         	else{ te++; sym.$("Ten").html(te%10);}
         
         	if(on == one + 30){ clearInterval(clockNum)}
         	else{ on++; sym.$("One").html(on%10);}
         },100);
         

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 12500, function(sym, e) {
         // insert code here
         if(isNowInAddtionalPage == true){
            isNowInAddtionalPage = false;
            sym.play(2995);
         }
      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 13000, function(sym, e) {
         // insert code here
         sym.stop();
         isNowInAddtionalPage = true;
      });
      //Edge binding end

      Symbol.bindSymbolAction(compId, symbolName, "creationComplete", function(sym, e) {
         //statusFlag表明了当前舞台的状态
         //不加var是全局变量哦
         //0表示加载完成，位于前三幅总体陈述页面中
         //1表示位于五幅具体招聘岗位中
         //2表示放映已结束
         statusFlag = 0;
         boardFlag = 0;
         guestNum = 0;
         
         xmlHttpRequest = new XMLHttpRequest();
         xmlHttpRequest.onreadystatechange = function(){
         	if(xmlHttpRequest.readyState == 4 && xmlHttpRequest.status == 200){    
                 var gotJson = JSON.parse(xmlHttpRequest.responseText);
                 var vistorNum = parseInt(gotJson.count);
                 guestNum = vistorNum;
            }  
         };
         xmlHttpRequest.open("GET","count.php",true);
         xmlHttpRequest.send();

      });
      //Edge binding end

      

      Symbol.bindElementAction(compId, symbolName, "${Stage}", "swipeleft", function(sym, e) {
         if( statusFlag == 1 && boardFlag == 0){
         	sym.play();
         }
         if(isNowInAddtionalPage == true){
            isNowInAddtionalPage = false;
            sym.play(3651);
         }

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${Stage}", "swiperight", function(sym, e) {
         if( statusFlag == 1 && boardFlag == 0){
         	sym.playReverse();
         }
         if(isNowInAddtionalPage == true){
            sym.playReverse();
         }

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 4250, function(sym, e) {
         sym.$("meteor_star").hide();
         statusFlag = 0;// insert code here

      });
      //Edge binding end

      

      Symbol.bindElementAction(compId, symbolName, "${Stage}", "touchmove", function(sym, e) {
         // insert code to be run when a user drags an object (for touch devices only)
         if(statusFlag == 0 && boardFlag == 0){
         	sym.play();	
         }

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${Stage}", "touchstart", function(sym, e) {
         // insert code to be run when a user touches the object (for touch devices only)
         event.preventDefault();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_4_amarillo_btn_pm}", "touchend", function(sym, e) {
         
         // 产品经理
         boardFlag = 1;
         sym.$("Title").html("产品经理");
         sym.$("PositionDutyDtl").html("负责产品规划、设计和项目实施，并负责产品的生命周期管理<br />分析项目、用户需求，分析竞争对手动态和市场动态，规划产品路线图，提出产品需求或改进意见<br />协调市场、开发、运营、管理等团队确立产品方案，跟踪产品开发进度，完成产品的开发、版本管理，评审发布，产品上线等相关工作<br />对产品需求进行版本管理，产品上线后改进等相关工作");
         sym.$("PositionRequirementDtl").html("三年以上互联网行业经验，精通互联网产品和服务，有独到的见解和认识<br />对产品设计，对产品分析、数据分析、竞争分析等有独到的见解，善于倾听用户声音<br />能够完成基本的产品设计，熟练使用设计软件和编写设计文档<br />思路清晰，较强的表达能力，良好的跨部门沟通和项目协调能力，强烈的结果导向意识<br />对工作充满激情，具有强烈的创新精神，富有责任心，能承受工作压力");
         sym.$("DetailPanel").show();
         

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${cross}", "touchend", function(sym, e) {
         // insert code to be run when a user stops touching an object (for touch devices only)
         // Hide an element 
         boardFlag = 0;
         sym.$("DetailPanel").hide();
         

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_4_naranja_btn_ios}", "touchend", function(sym, e) {
         // insert code to be run when a user stops touching an object (for touch devices only)
         // iOS
         boardFlag = 1;
         sym.$("Title").html("Android开发工程师");
         sym.$("PositionDutyDtl").html("根据团队要求快速开发新产品<br />对已有应用进行维护更新");
         sym.$("PositionRequirementDtl").html("有扎实的Java基础<br />精通Android开发中的交互、网络等技术<br />熟练使用Android开发工具和测试工具<br />有较强的学习和独立开发能力<br />至少有过一次完整的项目经历<br />全日制本科学历以上，研究生优先");
         sym.$("DetailPanel").show();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_4_naranja_btn_php}", "touchend", function(sym, e) {
         // insert code to be run when a user stops touching an object (for touch devices only)
         // php
         boardFlag = 1;
         sym.$("Title").html("网站开发工程师");
         sym.$("PositionDutyDtl").html("需求分析：了解竞品的功能、应用情况，比较优缺点；理解客户需求，定义产品可用性需求和使用场景，技术选型；<br />产品规划：协助产品经理和项目经理完成产品的生命周期管理、总体规划设计、定义产品功能、设计数据库存储等<br />模块与功能设计：编写模块功能、用例、流程等；编写概要设计、详细设计，进行数据库、类设计；制定测试计划<br />组织开发：组织开发团队，按照项目计划进行技术开发<br />质量保证：负责制定质量保证计划，负责最终的用户体验<br />沟通协调：负责与其它部门项目相关的协调工作<br />日常维护：负责公司运营平台技术维护工作");
         sym.$("PositionRequirementDtl").html("熟练使用HTML/CSS，可以独立进行设计图到静态代码的开发<br />熟练使用PHP，熟悉主流开发框架如WordPress，ThinkPHP等<br />熟练应用jQuery等JS前端开发框架，可以独立实现一般交互效果和功能<br />熟悉LAMP开发环境<br />熟悉MySQL<br />熟悉RESTful风格接口设计，具有产品化设计思想<br />有良好的文档撰写能力，逻辑分析能力、学习能力和创新能力强，具有团队合作精神<br />良好的语言表达及沟通能力<br />有基于WordPress网站或插件开发经验者优先");
         sym.$("DetailPanel").show();
         

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_4_naranja_btn_golang}", "touchend", function(sym, e) {
         // insert code to be run when a user stops touching an object (for touch devices only)
         // golang
         boardFlag = 1;
         sym.$("Title").html("Golang工程师");
         sym.$("PositionDutyDtl").html("系统架构设计和优化<br />产品功能设计和代码编写<br />数据库设计和开发");
         sym.$("PositionRequirementDtl").html("至少两年互联网产品开发经验；至少一年golang的开发经验<br />熟练使用Linux/Mac开发环境，熟悉基于Git的团队合作开发模式<br />熟悉postgresql、mongodb和redis<br />有自己的开源项目或者作为核心成员参与过其他开源项目的开发<br />熟悉php，python，nodejs等其他开发语言<br />对产品设计和交互有一定的研究<br />Fullstack潜质");
         sym.$("DetailPanel").show();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_4_naranja_btn_front}", "touchend", function(sym, e) {
         // insert code to be run when a user stops touching an object (for touch devices only)
         // 前段
         boardFlag = 1;
         sym.$("Title").html("前端开发工程师");
         sym.$("PositionDutyDtl").html("Web产品的功能设计、开发和实现<br />现有产品易用性改进和Web界面技术优化");
         sym.$("PositionRequirementDtl").html("统招本科以上学历，计算机相关专业<br />2年前端开发经验以上<br />了解典型的Server/Client/Browser架构<br />精通JavaScript，AJAX，HTML，CSS等技术<br />精通jQuery, backbone, angular.js等一种前端类库或框架<br />熟悉面向对象开发概念，具有学习和掌握新技术能力<br />了解一门服务器编程语言<br />了解HTML5，CSS3应用<br />了解Node.js");
         sym.$("DetailPanel").show();
         

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${Text}", "touchend", function(sym, e) {
         // insert code to be run when a user stops touching an object (for touch devices only)
         sym.play(0);

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_4_rojo_btn_maintain}", "touchend", function(sym, e) {
         // insert code to be run when a user stops touching an object (for touch devices only)
         // 运维
         boardFlag = 1;
         sym.$("Title").html("自动化测试工程师");
         sym.$("PositionDutyDtl").html("自动化测试方案、用例的编写<br />脚本的录制、编写及维护，自动化测试的执行，测试报告的编写<br />自动化测试技术研究、文档积累<br />bug分析和问题定位，跟踪并验证");
         sym.$("PositionRequirementDtl").html("计算机相关专业，有1年以上测试经验<br />熟悉jenkins ci框架，熟悉PHP，HTML，JS相关内容，熟悉性能测试、安全测试技术，熟悉性能建模原理<br />熟悉LoadRuner，selenium等自动化测试工具和框架<br />能够使用简洁明了的语言撰写测试报告<br />善于发现问题，站在用户的角度提出问题<br />沟通能力强，具有团队合作精神<br />具有较强的学习，沟通，协调，以及独立工作能力");
         sym.$("DetailPanel").show();
         

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_4_morado_btn_sm}", "touchend", function(sym, e) {
         // insert code to be run when a user stops touching an object (for touch devices only)
         // 新项目经理
         boardFlag = 1;
         sym.$("Title").html("售前售后技术支持工程师");
         sym.$("PositionDutyDtl").html("与客户进行售前咨询、售中和售后服务指引<br />负责跟踪并挖掘客户需求，根据需求促成订单，参与招投标工作<br />负责与客户进行交流及客户需求的分析调研，向客户提供售前咨询和售后培训、支持<br />协助技术部门完成客户的需求，提高客户的满意度<br />完成领导交代的其他任务");
         sym.$("PositionRequirementDtl").html("具有互联网行业、SaaS软件平台或OA、ERP、企业应用系统开发、销售、技术支持经验。有教育类行业经验者优先<br />有良好的文字组织能力，能够编写产品资料、招投标及其他技术方案<br />对技术支持工作有兴趣和信心，有进取心，有优秀的谈判以及口笔表达交流的技巧<br />有较强服务意识及团队协作能力，竞争与合作意识并存，执著有韧劲，能长期胜任一份工作");
         sym.$("DetailPanel").show();
         

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_4_azul_btn_ui}", "touchend", function(sym, e) {
         // insert code to be run when a user stops touching an object (for touch devices only)
         // ui
         boardFlag = 1;
         sym.$("Title").html("文案策划");
         sym.$("PositionDutyDtl").html("提供EDM、网络推广、微信推广的宣传软文，提升流量转化率；<br />完成各类文案的撰写工作，包括网站宣传资料、论坛帖子、软文类；<br />参与公司APP、微信、微博等自媒体广告创意、文案工作，与设计师一起完成广告的创意、主题与软文编辑。");
         sym.$("PositionRequirementDtl").html("一年以上IT行业工作经验；<br />热衷网络软文营销，文笔出众，撩拨人心，文章具备广泛传播效果；<br />具有较强的撰稿和文字编辑能力，深厚的文学功底，善于文案策划，对新闻有一定的敏感度；<br />思维活跃，见识广泛，视角独特，对新事物有着独到的看法和见解；<br />熟悉网络软文及新闻源软广告的写作技巧，擅长产品与品牌软文写作；<br />互联网公司2年以上编辑、写手或文案策划经验，优秀的文章写作能力；<br />熟悉网络文化，熟悉微信、微博内容传播特点，并善于结合大众用户心理需求、社会热点等营造话题，进行软文及原创文章撰写。");
         sym.$("DetailPanel").show();
         

      });
      //Edge binding end

      

      Symbol.bindElementAction(compId, symbolName, "${right_arrow}", "touchend", function(sym, e) {
         // insert code to be run when a user stops touching an object (for touch devices only)
         sym.play();
         

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_5_post_ps}", "touchend", function(sym, e) {
         sym.$("ps_describe").show();
         sym.$("ps_background").show();
         window.location.href="mailto:hr@tiup.us";

      });
      //Edge binding end

   })("stage");
   //Edge symbol end:'stage'

   //=========================================================
   
   //Edge symbol: 'meteor'
   (function(symbolName) {   
   
   })("meteor");
   //Edge symbol end:'meteor'

   //=========================================================
   
   //Edge symbol: 'meteor_star'
   (function(symbolName) {   
   
      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 6000, function(sym, e) {
         // insert code here
         sym.play(0);

      });
      //Edge binding end

   })("meteor_star");
   //Edge symbol end:'meteor_star'

})(window.jQuery || AdobeEdge.$, AdobeEdge, "EDGE-13547180");