/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='images/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "5.0.1",
                minimumCompatibleVersion: "5.0.0",
                build: "5.0.1.386",
                scaleToFit: "width",
                centerStage: "horizontal",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'Background',
                            type: 'image',
                            rect: ['0', '0', '640px', '1136px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",'img/background.jpg','0px','0px']
                        },
                        {
                            id: 'up_arrow2',
                            type: 'image',
                            rect: ['298px', '921px', '43px', '27px', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",'img/up_arrow.png','0px','0px']
                        },
                        {
                            id: '_1_stars3',
                            type: 'image',
                            rect: ['0', '0', '640px', '1136px', 'auto', 'auto'],
                            opacity: '1',
                            fill: ["rgba(0,0,0,0)",'img/1_stars.png','0px','0px']
                        },
                        {
                            id: '_1_title2',
                            type: 'image',
                            rect: ['18px', '134px', '603px', '219px', 'auto', 'auto'],
                            opacity: '1',
                            fill: ["rgba(0,0,0,0)",'img/1_title.png','0px','0px']
                        },
                        {
                            id: '_1_rocket2',
                            type: 'image',
                            rect: ['201px', '-650px', '238px', '702px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",'img/1_rocket.png','0px','0px']
                        },
                        {
                            id: '_1_startTravel',
                            type: 'text',
                            rect: ['248px', '879px', 'auto', 'auto', 'auto', 'auto'],
                            opacity: '1',
                            text: "开启旅程",
                            font: ['Arial, Helvetica, sans-serif', [36, "px"], "rgba(240,208,88,0.99)", "normal", "none", "", "break-word", "nowrap"],
                            transform: [[],[],[],[],['50%','55%']]
                        },
                        {
                            id: 'meteor_star',
                            symbolName: 'meteor_star',
                            display: 'none',
                            type: 'rect',
                            rect: ['97', '52', '267', '223', 'auto', 'auto']
                        },
                        {
                            id: '_2_describe',
                            type: 'image',
                            rect: ['153px', '750px', '333px', '194px', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",'img/2_describe.png','0px','0px']
                        },
                        {
                            id: '_2_plnet',
                            type: 'image',
                            rect: ['170px', '-400px', '300px', '378px', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",'img/2_plnet.png','0px','0px']
                        },
                        {
                            id: '_2_tags',
                            type: 'image',
                            rect: ['65px', '900px', '509px', '138px', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",'img/2_tags.png','0px','0px']
                        },
                        {
                            id: '_3_rocket',
                            type: 'image',
                            rect: ['-300px', '0px', '640px', '1136px', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",'img/3_rocket.png','0px','0px']
                        },
                        {
                            id: '_3_astronaut',
                            type: 'image',
                            rect: ['300px', '-82px', '640px', '1136px', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",'img/3_astronaut.png','0px','0px']
                        },
                        {
                            id: '_3_tags',
                            type: 'image',
                            rect: ['28px', '-76px', '584px', '276px', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",'img/3_tags.png','0px','0px']
                        },
                        {
                            id: '_3_title',
                            type: 'image',
                            rect: ['197px', '-162px', '246px', '86px', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",'img/3_title.png','0px','0px']
                        },
                        {
                            id: 'ground',
                            display: 'none',
                            type: 'image',
                            rect: ['0px', '200px', '640px', '1136px', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",'img/ground.png','0px','0px']
                        },
                        {
                            id: '_4_amarillo_robot_texted',
                            display: 'none',
                            type: 'image',
                            rect: ['300px', '-40px', '640px', '1136px', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",'img/4_amarillo_robot_texted.png','0px','0px']
                        },
                        {
                            id: '_4_amarillo_btn_pm',
                            display: 'none',
                            type: 'image',
                            rect: ['105px', '15px', '390px', '115px', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",'img/4_amarillo_btn_pm.png','0px','0px']
                        },
                        {
                            id: '_4_naranja_robot_texted',
                            display: 'none',
                            type: 'image',
                            rect: ['300px', '-25px', '640px', '1136px', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",'img/4_naranja_robot_texted.png','0px','0px']
                        },
                        {
                            id: '_4_naranja_btn_php',
                            display: 'none',
                            type: 'image',
                            rect: ['-150px', '295px', '254px', '103px', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",'img/4_naranja_btn_php.png','0px','0px']
                        },
                        {
                            id: '_4_naranja_btn_ios',
                            display: 'none',
                            type: 'image',
                            rect: ['571px', '171px', '261px', '94px', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",'img/4_naranja_btn_ios.png','0px','0px']
                        },
                        {
                            id: '_4_naranja_btn_golang',
                            display: 'none',
                            type: 'image',
                            rect: ['519px', '295px', '0px', '0px', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",'img/4_naranja_btn_golang.png','0px','0px']
                        },
                        {
                            id: '_4_naranja_btn_front',
                            display: 'none',
                            type: 'image',
                            rect: ['-149px', '168px', '288px', '99px', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",'img/4_naranja_btn_front.png','0px','0px']
                        },
                        {
                            id: '_4_rojo_robot_texted',
                            display: 'none',
                            type: 'image',
                            rect: ['300px', '-40px', '640px', '1136px', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",'img/4_rojo_robot_texted.png','0px','0px']
                        },
                        {
                            id: '_4_rojo_btn_maintain',
                            display: 'none',
                            type: 'image',
                            rect: ['399px', '232px', '442px', '143px', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",'img/4_rojo_btn_maintain.png','0px','0px']
                        },
                        {
                            id: '_4_morado_robot_texted',
                            display: 'none',
                            type: 'image',
                            rect: ['300px', '-25px', '640px', '1136px', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",'img/4_morado_robot_texted.png','0px','0px']
                        },
                        {
                            id: '_4_morado_btn_sm',
                            display: 'none',
                            type: 'image',
                            rect: ['399px', '232px', '442px', '143px', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",'img/4_morado_btn_sm.png','0px','0px']
                        },
                        {
                            id: '_4_azul_robot_texted',
                            display: 'none',
                            type: 'image',
                            rect: ['300px', '-71px', '640px', '1136px', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",'img/4_azul_robot_texted.png','0px','0px']
                        },
                        {
                            id: '_4_azul_btn_ui',
                            display: 'none',
                            type: 'image',
                            rect: ['300px', '183px', '442px', '143px', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",'img/4_azul_btn_ui.png','0px','0px']
                        },
                        {
                            id: '_5_sun',
                            display: 'none',
                            type: 'image',
                            rect: ['169px', '-50px', '521px', '371px', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",'img/5_sun.png','0px','0px']
                        },
                        {
                            id: '_5_post_ps',
                            display: 'none',
                            type: 'image',
                            rect: ['79px', '330px', '458px', '175px', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",'img/5_post_ps.png','0px','0px']
                        },
                        {
                            id: '_5_number_div',
                            display: 'none',
                            type: 'image',
                            rect: ['56px', '18px', '528px', '305px', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",'img/5_number_div.png','0px','0px']
                        },
                        {
                            id: '_5_ground',
                            display: 'none',
                            type: 'image',
                            rect: ['79px', '870px', '481px', '353px', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",'img/5_ground.png','0px','0px']
                        },
                        {
                            id: 'Nombro',
                            display: 'none',
                            type: 'group',
                            rect: ['94', '265', '454', '141', 'auto', 'auto'],
                            c: [
                            {
                                id: 'Thousand',
                                type: 'text',
                                rect: ['0px', '3px', 'auto', 'auto', 'auto', 'auto'],
                                text: "0",
                                align: "left",
                                font: ['Arial, Helvetica, sans-serif', [120, "px"], "rgba(220,170,52,0.99)", "800", "none solid rgb(0, 0, 0)", "normal", "break-word", "nowrap"]
                            },
                            {
                                id: 'Ten',
                                type: 'text',
                                rect: ['259px', '2px', 'auto', 'auto', 'auto', 'auto'],
                                text: "1",
                                align: "left",
                                font: ['Arial, Helvetica, sans-serif', [120, "px"], "rgba(220,170,52,0.9922)", "800", "none solid rgba(220, 170, 52, 0.992157)", "normal", "break-word", "nowrap"]
                            },
                            {
                                id: 'One',
                                type: 'text',
                                rect: ['387px', '0px', 'auto', 'auto', 'auto', 'auto'],
                                text: "9",
                                align: "left",
                                font: ['Arial, Helvetica, sans-serif', [120, "px"], "rgba(220,170,52,0.9961)", "800", "none solid rgba(220, 170, 52, 0.996078)", "normal", "break-word", "nowrap"]
                            },
                            {
                                id: 'Hundred',
                                type: 'text',
                                rect: ['131px', '1px', 'auto', 'auto', 'auto', 'auto'],
                                text: "6",
                                align: "left",
                                font: ['Arial, Helvetica, sans-serif', [120, "px"], "rgba(220,170,52,0.9922)", "800", "none solid rgba(220, 170, 52, 0.992157)", "normal", "break-word", "nowrap"]
                            }]
                        },
                        {
                            id: 'right_arrow',
                            display: 'none',
                            type: 'image',
                            rect: ['602px', '455px', '19px', '43px', 'auto', 'auto'],
                            opacity: '0.9',
                            fill: ["rgba(0,0,0,0)",'img/right_arrow.png','0px','0px']
                        },
                        {
                            id: 'ps_background',
                            display: 'block',
                            type: 'image',
                            rect: ['37px', '430px', '568px', '294px', 'auto', 'auto'],
                            opacity: '0.95',
                            fill: ["rgba(0,0,0,0)",'img/ps_background.png','0px','0px']
                        },
                        {
                            id: 'ps_describe',
                            display: 'block',
                            type: 'text',
                            rect: ['65px', '520px', '495px', '94px', 'auto', 'auto'],
                            text: "请寄送您的简历到<br>hr@tiup.us",
                            align: "center",
                            font: ['Arial, Helvetica, sans-serif', [40, "px"], "rgba(0,0,0,1)", "normal", "none", "normal", "break-word", ""]
                        },
                        {
                            id: 'Text',
                            display: 'none',
                            type: 'text',
                            rect: ['240px', '926px', 'auto', 'auto', 'auto', 'auto'],
                            text: "再看一遍",
                            font: ['Arial, Helvetica, sans-serif', [30, "px"], "rgba(4,36,72,0.99)", "normal", "none", "", "break-word", "nowrap"]
                        },
                        {
                            id: 'DetailPanel',
                            display: 'none',
                            type: 'group',
                            rect: ['0px', '0px', '640px', '1100px', 'auto', 'auto'],
                            overflow: 'visible',
                            title: 'detailPanel',
                            c: [
                            {
                                id: 'Rectangle',
                                type: 'rect',
                                rect: ['1px', '1px', '649px', '1100px', 'auto', 'auto'],
                                fill: ["rgba(248,208,16,0.9922)"],
                                stroke: [0,"rgba(0,0,0,1)","none"]
                            },
                            {
                                id: 'PositionDutyDtl',
                                type: 'text',
                                rect: ['33', '165px', '587px', '232px', 'auto', 'auto'],
                                text: "最多十行",
                                align: "left",
                                font: ['Arial, Helvetica, sans-serif', [22, "px"], "rgba(32,32,32,1)", "400", "none solid rgb(0, 0, 0)", "normal", "break-word", ""],
                                textStyle: ["", "", "32px", ""]
                            },
                            {
                                id: 'PositionRequirement',
                                type: 'text',
                                rect: ['33', '469px', 'auto', 'auto', 'auto', 'auto'],
                                text: "任职资格",
                                align: "left",
                                font: ['Arial, Helvetica, sans-serif', [32, "px"], "rgba(0,0,0,1)", "800", "none solid rgb(32, 32, 32)", "normal", "break-word", "nowrap"]
                            },
                            {
                                id: 'PositionRequirementDtl',
                                type: 'text',
                                rect: ['33px', '522px', '579px', '260px', 'auto', 'auto'],
                                text: "不要太长",
                                align: "left",
                                font: ['Arial, Helvetica, sans-serif', [22, "px"], "rgba(0,0,0,1)", "400", "none solid rgb(0, 0, 0)", "normal", "break-word", ""],
                                textStyle: ["", "", "32px", ""]
                            },
                            {
                                id: 'Title',
                                type: 'text',
                                rect: ['30px', '57px', '579px', '42px', 'auto', 'auto'],
                                text: "产品经理",
                                align: "center",
                                font: ['Arial, Helvetica, sans-serif', [36, "px"], "rgba(0,0,0,1)", "800", "none solid rgb(0, 0, 0)", "normal", "break-word", ""]
                            },
                            {
                                id: 'PositionDuty',
                                type: 'text',
                                rect: ['33px', '111px', 'auto', 'auto', 'auto', 'auto'],
                                text: "职位职责",
                                align: "left",
                                font: ['Arial, Helvetica, sans-serif', [32, "px"], "rgba(0,0,0,1)", "800", "none solid rgb(0, 0, 0)", "normal", "break-word", "nowrap"]
                            },
                            {
                                id: 'cross',
                                type: 'image',
                                rect: ['582px', '12px', '50px', '50px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",'img/cross.png','0px','0px'],
                                filter: [0, 0, 1, 1, 0, 0, 0, 0, "rgba(0,0,0,0)", 0, 0, 0],
                                transform: [[],[],[],['0.8','0.8']]
                            }]
                        },
                        {
                            id: '_6_rocket',
                            display: 'none',
                            type: 'image',
                            rect: ['100px', '400px', '176px', '142px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",'img/6_rocket.png','0px','0px']
                        },
                        {
                            id: '_6_planet',
                            display: 'none',
                            type: 'image',
                            rect: ['640px', '950px', '221px', '148px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",'img/6_planet.png','0px','0px']
                        },
                        {
                            id: '_6_partner',
                            display: 'none',
                            type: 'image',
                            rect: ['640px', '625px', '148px', '317px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",'img/6_partner.png','0px','0px']
                        },
                        {
                            id: '_6_partner_text',
                            display: 'none',
                            type: 'image',
                            rect: ['-560px', '700px', '436px', '142px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",'img/6_partner_text.png','0px','0px']
                        },
                        {
                            id: '_6_mission',
                            display: 'none',
                            type: 'image',
                            rect: ['640px', '60px', '120px', '308px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",'img/6_mission.png','0px','0px']
                        },
                        {
                            id: '_6_mission_text',
                            display: 'none',
                            type: 'image',
                            rect: ['-560px', '130px', '475px', '130px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",'img/6_mission_text.png','0px','0px']
                        },
                        {
                            id: '_6_journey',
                            display: 'none',
                            type: 'image',
                            rect: ['-120px', '340px', '113px', '317px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",'img/6_journey.png','0px','0px']
                        },
                        {
                            id: '_6_journey_text',
                            display: 'none',
                            type: 'image',
                            rect: ['640px', '420px', '450px', '158px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",'img/6_journey_text.png','0px','0px']
                        },
                        {
                            id: '_6_earth',
                            display: 'none',
                            type: 'image',
                            rect: ['0px', '1080px', '415px', '91px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",'img/6_earth.png','0px','0px']
                        },
                        {
                            id: '_6_astronaut_small',
                            display: 'none',
                            type: 'image',
                            rect: ['720px', '900px', '96px', '82px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",'img/6_astronaut_small.png','0px','0px']
                        },
                        {
                            id: '_6_astronaut_big',
                            display: 'none',
                            type: 'image',
                            rect: ['-400px', '750px', '370px', '369px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",'img/6_astronaut_big.png','0px','0px']
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '640px', '1100px', 'auto', 'auto'],
                            overflow: 'hidden',
                            fill: ["rgba(0,0,0,1.00)"]
                        }
                    }
                },
                timeline: {
                    duration: 13000,
                    autoPlay: true,
                    data: [
                        [
                            "eid185",
                            "top",
                            12000,
                            0,
                            "linear",
                            "${_3_rocket}",
                            '0px',
                            '0px'
                        ],
                        [
                            "eid198",
                            "display",
                            0,
                            0,
                            "linear",
                            "${DetailPanel}",
                            'none',
                            'none'
                        ],
                        [
                            "eid223",
                            "display",
                            4500,
                            0,
                            "linear",
                            "${DetailPanel}",
                            'none',
                            'none'
                        ],
                        [
                            "eid274",
                            "display",
                            12000,
                            0,
                            "linear",
                            "${DetailPanel}",
                            'none',
                            'none'
                        ],
                        [
                            "eid273",
                            "display",
                            0,
                            0,
                            "linear",
                            "${ps_describe}",
                            'block',
                            'none'
                        ],
                        [
                            "eid293",
                            "display",
                            0,
                            0,
                            "linear",
                            "${_6_mission_text}",
                            'none',
                            'none'
                        ],
                        [
                            "eid282",
                            "display",
                            12500,
                            0,
                            "linear",
                            "${_6_mission_text}",
                            'none',
                            'block'
                        ],
                        [
                            "eid292",
                            "display",
                            0,
                            0,
                            "linear",
                            "${_6_journey}",
                            'none',
                            'none'
                        ],
                        [
                            "eid281",
                            "display",
                            12500,
                            0,
                            "linear",
                            "${_6_journey}",
                            'none',
                            'block'
                        ],
                        [
                            "eid262",
                            "color",
                            7000,
                            0,
                            "linear",
                            "${PositionDuty}",
                            'rgba(0,0,0,1)',
                            'rgba(0,0,0,1)'
                        ],
                        [
                            "eid267",
                            "color",
                            7431,
                            0,
                            "linear",
                            "${PositionDuty}",
                            'rgba(0,0,0,1)',
                            'rgba(255,255,255,1.00)'
                        ],
                        [
                            "eid258",
                            "color",
                            8000,
                            0,
                            "linear",
                            "${PositionDuty}",
                            'rgba(255,255,255,1.00)',
                            'rgba(0,0,0,1)'
                        ],
                        [
                            "eid272",
                            "display",
                            0,
                            0,
                            "linear",
                            "${ps_background}",
                            'block',
                            'none'
                        ],
                        [
                            "eid6",
                            "scaleX",
                            0,
                            1000,
                            "linear",
                            "${_1_title2}",
                            '1',
                            '0.2'
                        ],
                        [
                            "eid43",
                            "opacity",
                            2000,
                            1000,
                            "linear",
                            "${_3_title}",
                            '0',
                            '1'
                        ],
                        [
                            "eid55",
                            "opacity",
                            3000,
                            1000,
                            "linear",
                            "${_3_title}",
                            '1',
                            '0'
                        ],
                        [
                            "eid338",
                            "left",
                            12500,
                            500,
                            "linear",
                            "${_6_journey}",
                            '-120px',
                            '60px'
                        ],
                        [
                            "eid22",
                            "top",
                            500,
                            1000,
                            "easeOutBounce",
                            "${_2_tags}",
                            '900px',
                            '731px'
                        ],
                        [
                            "eid29",
                            "top",
                            1500,
                            1000,
                            "linear",
                            "${_2_tags}",
                            '731px',
                            '900px'
                        ],
                        [
                            "eid210",
                            "display",
                            5000,
                            0,
                            "linear",
                            "${_4_naranja_btn_front}",
                            'none',
                            'block'
                        ],
                        [
                            "eid264",
                            "color",
                            7000,
                            0,
                            "linear",
                            "${PositionRequirementDtl}",
                            'rgba(0,0,0,1)',
                            'rgba(0,0,0,1)'
                        ],
                        [
                            "eid269",
                            "color",
                            7431,
                            0,
                            "linear",
                            "${PositionRequirementDtl}",
                            'rgba(0,0,0,1)',
                            'rgba(255,255,255,1.00)'
                        ],
                        [
                            "eid260",
                            "color",
                            8000,
                            0,
                            "linear",
                            "${PositionRequirementDtl}",
                            'rgba(255,255,255,1.00)',
                            'rgba(0,0,0,1)'
                        ],
                        [
                            "eid18",
                            "opacity",
                            500,
                            1000,
                            "linear",
                            "${_2_plnet}",
                            '0',
                            '1'
                        ],
                        [
                            "eid32",
                            "opacity",
                            1500,
                            1000,
                            "linear",
                            "${_2_plnet}",
                            '1',
                            '0'
                        ],
                        [
                            "eid97",
                            "opacity",
                            5000,
                            1000,
                            "linear",
                            "${_4_naranja_btn_front}",
                            '0',
                            '1'
                        ],
                        [
                            "eid112",
                            "opacity",
                            6000,
                            1000,
                            "linear",
                            "${_4_naranja_btn_front}",
                            '1',
                            '0'
                        ],
                        [
                            "eid203",
                            "display",
                            9500,
                            0,
                            "linear",
                            "${_4_azul_btn_ui}",
                            'none',
                            'block'
                        ],
                        [
                            "eid216",
                            "display",
                            3500,
                            0,
                            "linear",
                            "${_4_amarillo_btn_pm}",
                            'none',
                            'block'
                        ],
                        [
                            "eid224",
                            "background-color",
                            4500,
                            0,
                            "linear",
                            "${Rectangle}",
                            'rgba(248,208,16,0.9922)',
                            'rgba(248,208,16,0.9922)'
                        ],
                        [
                            "eid225",
                            "background-color",
                            5899,
                            0,
                            "linear",
                            "${Rectangle}",
                            'rgba(248,208,16,0.9922)',
                            'rgba(240,164,16,0.99)'
                        ],
                        [
                            "eid226",
                            "background-color",
                            7431,
                            0,
                            "linear",
                            "${Rectangle}",
                            'rgba(240,164,16,0.99)',
                            'rgba(236,72,60,0.99)'
                        ],
                        [
                            "eid227",
                            "background-color",
                            8931,
                            0,
                            "linear",
                            "${Rectangle}",
                            'rgba(236,72,60,0.99)',
                            'rgba(180,144,224,0.99)'
                        ],
                        [
                            "eid228",
                            "background-color",
                            10431,
                            0,
                            "linear",
                            "${Rectangle}",
                            'rgba(180,144,224,0.99)',
                            'rgba(32,188,248,0.99)'
                        ],
                        [
                            "eid343",
                            "left",
                            12500,
                            500,
                            "linear",
                            "${_6_planet}",
                            '640px',
                            '450px'
                        ],
                        [
                            "eid214",
                            "display",
                            5000,
                            0,
                            "linear",
                            "${_4_naranja_btn_php}",
                            'none',
                            'block'
                        ],
                        [
                            "eid19",
                            "opacity",
                            500,
                            1000,
                            "linear",
                            "${_2_describe}",
                            '0',
                            '1'
                        ],
                        [
                            "eid34",
                            "opacity",
                            1500,
                            1000,
                            "linear",
                            "${_2_describe}",
                            '1',
                            '0'
                        ],
                        [
                            "eid110",
                            "opacity",
                            5000,
                            1000,
                            "easeOutBack",
                            "${_4_naranja_robot_texted}",
                            '0',
                            '1'
                        ],
                        [
                            "eid116",
                            "opacity",
                            6000,
                            1000,
                            "linear",
                            "${_4_naranja_robot_texted}",
                            '1',
                            '0'
                        ],
                        [
                            "eid219",
                            "display",
                            3506,
                            0,
                            "linear",
                            "${ground}",
                            'none',
                            'block'
                        ],
                        [
                            "eid230",
                            "display",
                            11000,
                            0,
                            "linear",
                            "${ground}",
                            'block',
                            'none'
                        ],
                        [
                            "eid129",
                            "opacity",
                            6500,
                            1000,
                            "linear",
                            "${_4_rojo_robot_texted}",
                            '0',
                            '1'
                        ],
                        [
                            "eid133",
                            "opacity",
                            7500,
                            1000,
                            "easeOutBack",
                            "${_4_rojo_robot_texted}",
                            '1',
                            '0'
                        ],
                        [
                            "eid252",
                            "display",
                            1500,
                            0,
                            "linear",
                            "${meteor_star}",
                            'none',
                            'block'
                        ],
                        [
                            "eid8",
                            "opacity",
                            0,
                            1000,
                            "linear",
                            "${_1_title2}",
                            '1',
                            '0'
                        ],
                        [
                            "eid28",
                            "top",
                            500,
                            1000,
                            "easeOutBounce",
                            "${_2_describe}",
                            '750px',
                            '506px'
                        ],
                        [
                            "eid33",
                            "top",
                            1500,
                            1000,
                            "linear",
                            "${_2_describe}",
                            '506px',
                            '750px'
                        ],
                        [
                            "eid50",
                            "top",
                            2000,
                            1000,
                            "easeOutBounce",
                            "${_3_title}",
                            '-162px',
                            '38px'
                        ],
                        [
                            "eid54",
                            "top",
                            3000,
                            1000,
                            "linear",
                            "${_3_title}",
                            '38px',
                            '-162px'
                        ],
                        [
                            "eid141",
                            "opacity",
                            8000,
                            1000,
                            "linear",
                            "${_4_morado_robot_texted}",
                            '0',
                            '1'
                        ],
                        [
                            "eid145",
                            "opacity",
                            9000,
                            1000,
                            "easeOutBack",
                            "${_4_morado_robot_texted}",
                            '1',
                            '0'
                        ],
                        [
                            "eid2",
                            "top",
                            0,
                            1000,
                            "linear",
                            "${_1_rocket2}",
                            '198px',
                            '-650px'
                        ],
                        [
                            "eid342",
                            "left",
                            12500,
                            500,
                            "linear",
                            "${_6_partner}",
                            '640px',
                            '500px'
                        ],
                        [
                            "eid341",
                            "left",
                            12500,
                            500,
                            "linear",
                            "${_6_partner_text}",
                            '-560px',
                            '60px'
                        ],
                        [
                            "eid340",
                            "left",
                            12500,
                            500,
                            "linear",
                            "${_6_mission}",
                            '640px',
                            '520px'
                        ],
                        [
                            "eid184",
                            "opacity",
                            1500,
                            1000,
                            "easeOutBounce",
                            "${_1_stars3}",
                            '1',
                            '0'
                        ],
                        [
                            "eid294",
                            "display",
                            0,
                            0,
                            "linear",
                            "${_6_mission}",
                            'none',
                            'none'
                        ],
                        [
                            "eid283",
                            "display",
                            12500,
                            0,
                            "linear",
                            "${_6_mission}",
                            'none',
                            'block'
                        ],
                        [
                            "eid337",
                            "left",
                            12500,
                            500,
                            "linear",
                            "${_6_journey_text}",
                            '640px',
                            '160px'
                        ],
                        [
                            "eid98",
                            "opacity",
                            5000,
                            1000,
                            "linear",
                            "${_4_naranja_btn_golang}",
                            '0',
                            '1'
                        ],
                        [
                            "eid113",
                            "opacity",
                            6000,
                            1000,
                            "linear",
                            "${_4_naranja_btn_golang}",
                            '1',
                            '0'
                        ],
                        [
                            "eid140",
                            "opacity",
                            8000,
                            1000,
                            "linear",
                            "${_4_morado_btn_sm}",
                            '0',
                            '1'
                        ],
                        [
                            "eid144",
                            "opacity",
                            9000,
                            1000,
                            "easeOutBack",
                            "${_4_morado_btn_sm}",
                            '1',
                            '0'
                        ],
                        [
                            "eid106",
                            "left",
                            5000,
                            1000,
                            "easeOutBack",
                            "${_4_naranja_btn_golang}",
                            '519px',
                            '320px'
                        ],
                        [
                            "eid122",
                            "left",
                            6000,
                            1000,
                            "linear",
                            "${_4_naranja_btn_golang}",
                            '320px',
                            '119px'
                        ],
                        [
                            "eid211",
                            "display",
                            5000,
                            0,
                            "linear",
                            "${_4_naranja_btn_golang}",
                            'none',
                            'block'
                        ],
                        [
                            "eid336",
                            "left",
                            12500,
                            500,
                            "linear",
                            "${_6_astronaut_small}",
                            '720px',
                            '410px'
                        ],
                        [
                            "eid335",
                            "left",
                            12500,
                            500,
                            "linear",
                            "${_6_astronaut_big}",
                            '-400px',
                            '0px'
                        ],
                        [
                            "eid152",
                            "opacity",
                            9500,
                            1000,
                            "linear",
                            "${_4_azul_btn_ui}",
                            '0',
                            '1'
                        ],
                        [
                            "eid156",
                            "opacity",
                            10500,
                            1000,
                            "linear",
                            "${_4_azul_btn_ui}",
                            '1',
                            '0'
                        ],
                        [
                            "eid312",
                            "display",
                            0,
                            0,
                            "linear",
                            "${Nombro}",
                            'none',
                            'none'
                        ],
                        [
                            "eid310",
                            "display",
                            11000,
                            0,
                            "linear",
                            "${Nombro}",
                            'none',
                            'none'
                        ],
                        [
                            "eid311",
                            "display",
                            11945,
                            0,
                            "linear",
                            "${Nombro}",
                            'none',
                            'block'
                        ],
                        [
                            "eid256",
                            "display",
                            12000,
                            0,
                            "linear",
                            "${Nombro}",
                            'block',
                            'none'
                        ],
                        [
                            "eid288",
                            "display",
                            0,
                            0,
                            "linear",
                            "${_6_astronaut_big}",
                            'none',
                            'none'
                        ],
                        [
                            "eid277",
                            "display",
                            12500,
                            0,
                            "linear",
                            "${_6_astronaut_big}",
                            'none',
                            'block'
                        ],
                        [
                            "eid105",
                            "left",
                            5000,
                            1000,
                            "easeOutBack",
                            "${_4_naranja_btn_front}",
                            '-149px',
                            '41px'
                        ],
                        [
                            "eid123",
                            "left",
                            6000,
                            1000,
                            "linear",
                            "${_4_naranja_btn_front}",
                            '41px',
                            '-149px'
                        ],
                        [
                            "eid17",
                            "opacity",
                            500,
                            1000,
                            "linear",
                            "${_2_tags}",
                            '0',
                            '1'
                        ],
                        [
                            "eid30",
                            "opacity",
                            1500,
                            1000,
                            "linear",
                            "${_2_tags}",
                            '1',
                            '0'
                        ],
                        [
                            "eid72",
                            "opacity",
                            3500,
                            1000,
                            "linear",
                            "${_4_amarillo_robot_texted}",
                            '0',
                            '1'
                        ],
                        [
                            "eid84",
                            "opacity",
                            4500,
                            1000,
                            "linear",
                            "${_4_amarillo_robot_texted}",
                            '1',
                            '0'
                        ],
                        [
                            "eid77",
                            "left",
                            3500,
                            1000,
                            "easeOutQuad",
                            "${_4_amarillo_robot_texted}",
                            '300px',
                            '0px'
                        ],
                        [
                            "eid83",
                            "left",
                            4500,
                            1000,
                            "linear",
                            "${_4_amarillo_robot_texted}",
                            '0px',
                            '-300px'
                        ],
                        [
                            "eid175",
                            "top",
                            11000,
                            1000,
                            "easeOutBounce",
                            "${_5_number_div}",
                            '18px',
                            '218px'
                        ],
                        [
                            "eid178",
                            "top",
                            11000,
                            1000,
                            "linear",
                            "${_5_sun}",
                            '-50px',
                            '0px'
                        ],
                        [
                            "eid27",
                            "top",
                            500,
                            1000,
                            "easeOutBounce",
                            "${_2_plnet}",
                            '-400px',
                            '83px'
                        ],
                        [
                            "eid31",
                            "top",
                            1500,
                            1000,
                            "linear",
                            "${_2_plnet}",
                            '83px',
                            '-400px'
                        ],
                        [
                            "eid204",
                            "display",
                            9500,
                            0,
                            "linear",
                            "${_4_azul_robot_texted}",
                            'none',
                            'block'
                        ],
                        [
                            "eid296",
                            "display",
                            0,
                            0,
                            "linear",
                            "${_6_partner}",
                            'none',
                            'none'
                        ],
                        [
                            "eid285",
                            "display",
                            12500,
                            0,
                            "linear",
                            "${_6_partner}",
                            'none',
                            'block'
                        ],
                        [
                            "eid339",
                            "left",
                            12500,
                            500,
                            "linear",
                            "${_6_mission_text}",
                            '-560px',
                            '60px'
                        ],
                        [
                            "eid49",
                            "opacity",
                            2000,
                            1000,
                            "linear",
                            "${_3_rocket}",
                            '0',
                            '1'
                        ],
                        [
                            "eid60",
                            "opacity",
                            3000,
                            1000,
                            "linear",
                            "${_3_rocket}",
                            '1',
                            '0'
                        ],
                        [
                            "eid47",
                            "opacity",
                            2000,
                            1000,
                            "linear",
                            "${_3_tags}",
                            '0',
                            '1'
                        ],
                        [
                            "eid58",
                            "opacity",
                            3000,
                            1000,
                            "linear",
                            "${_3_tags}",
                            '1',
                            '0'
                        ],
                        [
                            "eid143",
                            "left",
                            8000,
                            1000,
                            "easeOutBack",
                            "${_4_morado_btn_sm}",
                            '399px',
                            '99px'
                        ],
                        [
                            "eid147",
                            "left",
                            9000,
                            1000,
                            "linear",
                            "${_4_morado_btn_sm}",
                            '99px',
                            '-201px'
                        ],
                        [
                            "eid154",
                            "left",
                            9500,
                            1000,
                            "easeOutBack",
                            "${_4_azul_robot_texted}",
                            '300px',
                            '0px'
                        ],
                        [
                            "eid158",
                            "left",
                            10500,
                            1000,
                            "linear",
                            "${_4_azul_robot_texted}",
                            '0px',
                            '-300px'
                        ],
                        [
                            "eid176",
                            "top",
                            11000,
                            1000,
                            "easeOutBounce",
                            "${_5_post_ps}",
                            '330px',
                            '530px'
                        ],
                        [
                            "eid74",
                            "opacity",
                            3500,
                            1000,
                            "linear",
                            "${_4_amarillo_btn_pm}",
                            '0',
                            '1'
                        ],
                        [
                            "eid79",
                            "opacity",
                            4500,
                            1000,
                            "linear",
                            "${_4_amarillo_btn_pm}",
                            '1',
                            '0'
                        ],
                        [
                            "eid109",
                            "left",
                            5000,
                            1000,
                            "easeOutBack",
                            "${_4_naranja_btn_php}",
                            '-150px',
                            '200px'
                        ],
                        [
                            "eid118",
                            "left",
                            6000,
                            1000,
                            "linear",
                            "${_4_naranja_btn_php}",
                            '200px',
                            '-150px'
                        ],
                        [
                            "eid220",
                            "display",
                            11000,
                            0,
                            "linear",
                            "${_5_ground}",
                            'none',
                            'block'
                        ],
                        [
                            "eid302",
                            "display",
                            12000,
                            0,
                            "linear",
                            "${_5_ground}",
                            'block',
                            'none'
                        ],
                        [
                            "eid172",
                            "opacity",
                            11000,
                            1000,
                            "linear",
                            "${_5_post_ps}",
                            '0',
                            '1'
                        ],
                        [
                            "eid7",
                            "scaleY",
                            0,
                            1000,
                            "linear",
                            "${_1_title2}",
                            '1',
                            '0.2'
                        ],
                        [
                            "eid275",
                            "line-height",
                            12000,
                            0,
                            "linear",
                            "${PositionDutyDtl}",
                            '32px',
                            '32px'
                        ],
                        [
                            "eid188",
                            "display",
                            0,
                            0,
                            "linear",
                            "${Text}",
                            'none',
                            'none'
                        ],
                        [
                            "eid187",
                            "display",
                            12000,
                            0,
                            "linear",
                            "${Text}",
                            'none',
                            'block'
                        ],
                        [
                            "eid73",
                            "opacity",
                            3500,
                            1000,
                            "linear",
                            "${ground}",
                            '0',
                            '1'
                        ],
                        [
                            "eid297",
                            "display",
                            0,
                            0,
                            "linear",
                            "${_6_planet}",
                            'none',
                            'none'
                        ],
                        [
                            "eid286",
                            "display",
                            12500,
                            0,
                            "linear",
                            "${_6_planet}",
                            'none',
                            'block'
                        ],
                        [
                            "eid207",
                            "display",
                            8000,
                            0,
                            "linear",
                            "${_4_morado_robot_texted}",
                            'none',
                            'block'
                        ],
                        [
                            "eid104",
                            "opacity",
                            5000,
                            1000,
                            "linear",
                            "${_4_naranja_btn_php}",
                            '0',
                            '1'
                        ],
                        [
                            "eid119",
                            "opacity",
                            6000,
                            1000,
                            "linear",
                            "${_4_naranja_btn_php}",
                            '1',
                            '0'
                        ],
                        [
                            "eid48",
                            "opacity",
                            2000,
                            1000,
                            "linear",
                            "${_3_astronaut}",
                            '0',
                            '1'
                        ],
                        [
                            "eid59",
                            "opacity",
                            3000,
                            1000,
                            "linear",
                            "${_3_astronaut}",
                            '1',
                            '0'
                        ],
                        [
                            "eid111",
                            "left",
                            5000,
                            1000,
                            "easeOutBack",
                            "${_4_naranja_robot_texted}",
                            '300px',
                            '0px'
                        ],
                        [
                            "eid117",
                            "left",
                            6000,
                            1000,
                            "linear",
                            "${_4_naranja_robot_texted}",
                            '0px',
                            '-300px'
                        ],
                        [
                            "eid171",
                            "opacity",
                            11000,
                            1000,
                            "linear",
                            "${_5_number_div}",
                            '0',
                            '1'
                        ],
                        [
                            "eid181",
                            "opacity",
                            0,
                            0,
                            "easeOutBounce",
                            "${up_arrow2}",
                            '1',
                            '1'
                        ],
                        [
                            "eid180",
                            "opacity",
                            3000,
                            0,
                            "easeOutBounce",
                            "${up_arrow2}",
                            '1',
                            '0'
                        ],
                        [
                            "eid218",
                            "display",
                            3506,
                            0,
                            "linear",
                            "${_4_amarillo_robot_texted}",
                            'none',
                            'block'
                        ],
                        [
                            "eid103",
                            "opacity",
                            5000,
                            1000,
                            "linear",
                            "${_4_naranja_btn_ios}",
                            '0',
                            '1'
                        ],
                        [
                            "eid115",
                            "opacity",
                            6000,
                            1000,
                            "linear",
                            "${_4_naranja_btn_ios}",
                            '1',
                            '0'
                        ],
                        [
                            "eid142",
                            "left",
                            8000,
                            1000,
                            "easeOutBack",
                            "${_4_morado_robot_texted}",
                            '300px',
                            '0px'
                        ],
                        [
                            "eid146",
                            "left",
                            9000,
                            1000,
                            "linear",
                            "${_4_morado_robot_texted}",
                            '0px',
                            '-300px'
                        ],
                        [
                            "eid177",
                            "left",
                            11000,
                            1000,
                            "linear",
                            "${_5_sun}",
                            '169px',
                            '119px'
                        ],
                        [
                            "eid53",
                            "left",
                            2000,
                            1000,
                            "linear",
                            "${_3_rocket}",
                            '-300px',
                            '0px'
                        ],
                        [
                            "eid61",
                            "left",
                            3000,
                            1000,
                            "linear",
                            "${_3_rocket}",
                            '0px',
                            '-300px'
                        ],
                        [
                            "eid186",
                            "left",
                            4000,
                            500,
                            "linear",
                            "${_3_rocket}",
                            '-300px',
                            '-299px'
                        ],
                        [
                            "eid222",
                            "left",
                            4500,
                            7500,
                            "linear",
                            "${_3_rocket}",
                            '-299px',
                            '-299px'
                        ],
                        [
                            "eid75",
                            "top",
                            3500,
                            1000,
                            "easeOutBounce",
                            "${_4_amarillo_btn_pm}",
                            '15px',
                            '190px'
                        ],
                        [
                            "eid80",
                            "top",
                            4500,
                            1000,
                            "linear",
                            "${_4_amarillo_btn_pm}",
                            '190px',
                            '15px'
                        ],
                        [
                            "eid108",
                            "left",
                            5000,
                            1000,
                            "easeOutBack",
                            "${_4_naranja_btn_ios}",
                            '571px',
                            '341px'
                        ],
                        [
                            "eid120",
                            "left",
                            6000,
                            1000,
                            "linear",
                            "${_4_naranja_btn_ios}",
                            '341px',
                            '171px'
                        ],
                        [
                            "eid201",
                            "display",
                            11000,
                            0,
                            "linear",
                            "${_5_sun}",
                            'block',
                            'block'
                        ],
                        [
                            "eid300",
                            "display",
                            12000,
                            0,
                            "linear",
                            "${_5_sun}",
                            'block',
                            'none'
                        ],
                        [
                            "eid266",
                            "color",
                            7000,
                            0,
                            "linear",
                            "${PositionDutyDtl}",
                            'rgba(32,32,32,1)',
                            'rgba(32,32,32,1)'
                        ],
                        [
                            "eid271",
                            "color",
                            7431,
                            0,
                            "linear",
                            "${PositionDutyDtl}",
                            'rgba(32,32,32,1)',
                            'rgba(255,255,255,1.00)'
                        ],
                        [
                            "eid257",
                            "color",
                            8000,
                            0,
                            "linear",
                            "${PositionDutyDtl}",
                            'rgba(255,255,255,1.00)',
                            'rgba(32,32,32,1)'
                        ],
                        [
                            "eid290",
                            "display",
                            0,
                            0,
                            "linear",
                            "${_6_earth}",
                            'none',
                            'none'
                        ],
                        [
                            "eid279",
                            "display",
                            12500,
                            0,
                            "linear",
                            "${_6_earth}",
                            'none',
                            'block'
                        ],
                        [
                            "eid295",
                            "display",
                            0,
                            0,
                            "linear",
                            "${_6_partner_text}",
                            'none',
                            'none'
                        ],
                        [
                            "eid284",
                            "display",
                            12500,
                            0,
                            "linear",
                            "${_6_partner_text}",
                            'none',
                            'block'
                        ],
                        [
                            "eid131",
                            "left",
                            6500,
                            1000,
                            "easeOutBack",
                            "${_4_rojo_btn_maintain}",
                            '399px',
                            '99px'
                        ],
                        [
                            "eid135",
                            "left",
                            7500,
                            1000,
                            "linear",
                            "${_4_rojo_btn_maintain}",
                            '99px',
                            '-201px'
                        ],
                        [
                            "eid213",
                            "display",
                            5000,
                            0,
                            "linear",
                            "${_4_naranja_btn_ios}",
                            'none',
                            'block'
                        ],
                        [
                            "eid209",
                            "display",
                            6500,
                            0,
                            "linear",
                            "${_4_rojo_robot_texted}",
                            'none',
                            'block'
                        ],
                        [
                            "eid263",
                            "color",
                            7000,
                            0,
                            "linear",
                            "${Title}",
                            'rgba(0,0,0,1)',
                            'rgba(0,0,0,1)'
                        ],
                        [
                            "eid268",
                            "color",
                            7431,
                            0,
                            "linear",
                            "${Title}",
                            'rgba(0,0,0,1)',
                            'rgba(255,255,255,1.00)'
                        ],
                        [
                            "eid259",
                            "color",
                            8000,
                            0,
                            "linear",
                            "${Title}",
                            'rgba(255,255,255,1.00)',
                            'rgba(0,0,0,1)'
                        ],
                        [
                            "eid51",
                            "top",
                            2000,
                            1000,
                            "easeOutBounce",
                            "${_3_tags}",
                            '-76px',
                            '124px'
                        ],
                        [
                            "eid56",
                            "top",
                            3000,
                            1000,
                            "linear",
                            "${_3_tags}",
                            '124px',
                            '-76px'
                        ],
                        [
                            "eid289",
                            "display",
                            0,
                            0,
                            "linear",
                            "${_6_astronaut_small}",
                            'none',
                            'none'
                        ],
                        [
                            "eid278",
                            "display",
                            12500,
                            0,
                            "linear",
                            "${_6_astronaut_small}",
                            'none',
                            'block'
                        ],
                        [
                            "eid199",
                            "display",
                            11000,
                            0,
                            "linear",
                            "${_5_number_div}",
                            'none',
                            'block'
                        ],
                        [
                            "eid301",
                            "display",
                            12000,
                            0,
                            "linear",
                            "${_5_number_div}",
                            'block',
                            'none'
                        ],
                        [
                            "eid174",
                            "top",
                            11000,
                            1000,
                            "easeOutBounce",
                            "${_5_ground}",
                            '870px',
                            '670px'
                        ],
                        [
                            "eid254",
                            "display",
                            4500,
                            0,
                            "linear",
                            "${right_arrow}",
                            'none',
                            'block'
                        ],
                        [
                            "eid255",
                            "display",
                            11000,
                            0,
                            "linear",
                            "${right_arrow}",
                            'block',
                            'none'
                        ],
                        [
                            "eid291",
                            "display",
                            0,
                            0,
                            "linear",
                            "${_6_journey_text}",
                            'none',
                            'none'
                        ],
                        [
                            "eid280",
                            "display",
                            12500,
                            0,
                            "linear",
                            "${_6_journey_text}",
                            'none',
                            'block'
                        ],
                        [
                            "eid265",
                            "color",
                            7000,
                            0,
                            "linear",
                            "${PositionRequirement}",
                            'rgba(0,0,0,1)',
                            'rgba(0,0,0,1)'
                        ],
                        [
                            "eid270",
                            "color",
                            7431,
                            0,
                            "linear",
                            "${PositionRequirement}",
                            'rgba(0,0,0,1)',
                            'rgba(255,255,255,1.00)'
                        ],
                        [
                            "eid261",
                            "color",
                            8000,
                            0,
                            "linear",
                            "${PositionRequirement}",
                            'rgba(255,255,255,1.00)',
                            'rgba(0,0,0,1)'
                        ],
                        [
                            "eid155",
                            "left",
                            9500,
                            1000,
                            "easeOutBack",
                            "${_4_azul_btn_ui}",
                            '300px',
                            '94px'
                        ],
                        [
                            "eid157",
                            "left",
                            10500,
                            1000,
                            "linear",
                            "${_4_azul_btn_ui}",
                            '94px',
                            '-204px'
                        ],
                        [
                            "eid10",
                            "opacity",
                            36,
                            1000,
                            "linear",
                            "${_1_startTravel}",
                            '1',
                            '0'
                        ],
                        [
                            "eid128",
                            "opacity",
                            6500,
                            1000,
                            "linear",
                            "${_4_rojo_btn_maintain}",
                            '0',
                            '1'
                        ],
                        [
                            "eid132",
                            "opacity",
                            7500,
                            1000,
                            "easeOutBack",
                            "${_4_rojo_btn_maintain}",
                            '1',
                            '0'
                        ],
                        [
                            "eid345",
                            "top",
                            12500,
                            500,
                            "linear",
                            "${_6_rocket}",
                            '400px',
                            '250px'
                        ],
                        [
                            "eid206",
                            "display",
                            8000,
                            0,
                            "linear",
                            "${_4_morado_btn_sm}",
                            'none',
                            'block'
                        ],
                        [
                            "eid78",
                            "top",
                            3500,
                            1000,
                            "easeOutBounce",
                            "${ground}",
                            '200px',
                            '0px'
                        ],
                        [
                            "eid170",
                            "opacity",
                            11000,
                            1000,
                            "linear",
                            "${_5_ground}",
                            '0',
                            '1'
                        ],
                        [
                            "eid52",
                            "left",
                            2000,
                            1000,
                            "linear",
                            "${_3_astronaut}",
                            '300px',
                            '0px'
                        ],
                        [
                            "eid57",
                            "left",
                            3000,
                            1000,
                            "linear",
                            "${_3_astronaut}",
                            '0px',
                            '300px'
                        ],
                        [
                            "eid208",
                            "display",
                            6500,
                            0,
                            "linear",
                            "${_4_rojo_btn_maintain}",
                            'none',
                            'block'
                        ],
                        [
                            "eid153",
                            "opacity",
                            9500,
                            1000,
                            "linear",
                            "${_4_azul_robot_texted}",
                            '0',
                            '1'
                        ],
                        [
                            "eid159",
                            "opacity",
                            10500,
                            1000,
                            "linear",
                            "${_4_azul_robot_texted}",
                            '1',
                            '0'
                        ],
                        [
                            "eid200",
                            "display",
                            11000,
                            0,
                            "linear",
                            "${_5_post_ps}",
                            'none',
                            'block'
                        ],
                        [
                            "eid299",
                            "display",
                            12000,
                            0,
                            "linear",
                            "${_5_post_ps}",
                            'block',
                            'none'
                        ],
                        [
                            "eid215",
                            "display",
                            5000,
                            0,
                            "linear",
                            "${_4_naranja_robot_texted}",
                            'none',
                            'block'
                        ],
                        [
                            "eid173",
                            "opacity",
                            11000,
                            1000,
                            "linear",
                            "${_5_sun}",
                            '0',
                            '1'
                        ],
                        [
                            "eid130",
                            "left",
                            6500,
                            1000,
                            "easeOutBack",
                            "${_4_rojo_robot_texted}",
                            '300px',
                            '0px'
                        ],
                        [
                            "eid134",
                            "left",
                            7500,
                            1000,
                            "linear",
                            "${_4_rojo_robot_texted}",
                            '0px',
                            '-300px'
                        ],
                        [
                            "eid298",
                            "display",
                            0,
                            0,
                            "linear",
                            "${_6_rocket}",
                            'none',
                            'none'
                        ],
                        [
                            "eid287",
                            "display",
                            12500,
                            0,
                            "linear",
                            "${_6_rocket}",
                            'none',
                            'block'
                        ],
                            [ "eid253", "trigger", 500, function executeSymbolFunction(e, data) { this._executeSymbolAction(e, data); }, ['play', '${meteor_star}', [0] ] ]
                    ]
                }
            },
            "meteor": {
                version: "5.0.1",
                minimumCompatibleVersion: "5.0.0",
                build: "5.0.1.386",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [

                    ],
                    style: {
                        '${symbolSelector}': {
                            rect: [null, null, '267px', '223px']
                        }
                    }
                },
                timeline: {
                    duration: 0,
                    autoPlay: true,
                    data: [

                    ]
                }
            },
            "meteor_star": {
                version: "5.0.1",
                minimumCompatibleVersion: "5.0.0",
                build: "5.0.1.386",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            rect: ['197px', '-162px', '267px', '223px', 'auto', 'auto'],
                            id: 'meteor',
                            type: 'image',
                            fill: ['rgba(0,0,0,0)', 'img/meteor.png', '0px', '0px']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            rect: [null, null, '267px', '223px']
                        }
                    }
                },
                timeline: {
                    duration: 6000,
                    autoPlay: false,
                    data: [
                        [
                            "eid248",
                            "top",
                            0,
                            1000,
                            "linear",
                            "${meteor}",
                            '-162px',
                            '301px'
                        ],
                        [
                            "eid250",
                            "top",
                            6000,
                            0,
                            "linear",
                            "${meteor}",
                            '301px',
                            '244px'
                        ],
                        [
                            "eid247",
                            "left",
                            0,
                            1000,
                            "linear",
                            "${meteor}",
                            '197px',
                            '-359px'
                        ],
                        [
                            "eid249",
                            "left",
                            6000,
                            0,
                            "linear",
                            "${meteor}",
                            '-359px',
                            '-290px'
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

    if (!window.edge_authoring_mode) AdobeEdge.getComposition(compId).load("hire_edgeActions.js");
})("EDGE-13547180");
